setenv CAML_LD_LIBRARY_PATH "$HOME/.opam/system/lib/stublibs:/usr/local/lib/ocaml/stublibs";
setenv PERL5LIB "$HOME/.opam/system/lib/perl5:$PERL5LIB";
setenv OCAML_TOPLEVEL_PATH "$HOME/.opam/system/lib/toplevel";
setenv MANPATH "$MANPATH:$HOME/.opam/system/man";
setenv PATH "$HOME/.opam/system/bin:$PATH";

CAML_LD_LIBRARY_PATH=$HOME/.opam/system/lib/stublibs:/usr/local/lib/ocaml/stublibs; export CAML_LD_LIBRARY_PATH;
PERL5LIB=$HOME/.opam/system/lib/perl5:$PERL5LIB; export PERL5LIB;
OCAML_TOPLEVEL_PATH=$HOME/.opam/system/lib/toplevel; export OCAML_TOPLEVEL_PATH;
MANPATH=$MANPATH:$HOME/.opam/system/man; export MANPATH;
PATH=$HOME/.opam/system/bin:$PATH; export PATH;
